
// chatRooms is a hash, with the keys being the names of the chatrooms and the
// value being a hash with keys being the, creator name, names of the nicknames and messages
// and the values being the creator, nicknames and messages arrays, respectively
var chatRooms = {};
var privateMessages = {};
var bannedLists = {};
var currentOnlineUsers = new Array();

function addUserToChatRoom(nickname, chatRoom){
	console.log("in addUserToChatRoom -- nickname: " + nickname + " chatRoom: " + chatRoom);
	
	chatRooms[chatRoom]["nicknames"].push(nickname);
	
	// appears to be working correctly
}

// returns true for successfully added chatroom
function addChatRoom(chatRoom, creator_nickname, roomPassword){
	console.log("adding the chatroom with name: " + chatRoom + " creator: " + creator_nickname);

	if(chatRoomExists(chatRoom)){
		console.log("chatroom existed, so adding chatroom failed");
		return false;
	}
	
	console.log("chatroom did not exist");
	
	var arr = {};
	var nicknames = new Array();
	var messages = new Array();
			
	arr["nicknames"] = nicknames;
	arr["messages"] = messages;
	arr["creator"] = creator_nickname;
	if (roomPassword != null) { //if the room has a password
      console.log("adding password");
      arr["password"] = roomPassword;
    }
	chatRooms[chatRoom] = arr;
	
	console.log("chatRoom name: " + chatRoom);
	
	// chatRoom[chatRoom] is undefined
	console.log("chatRoom creator: " + chatRooms[chatRoom]["creator"]);
	
	return true;
}

function leaveChatroom(nickname, chatroom_name){
	console.log(chatRooms[chatroom_name]["nicknames"]);
	var index = chatRooms[chatroom_name]["nicknames"].indexOf(nickname);
	console.log(index);
	if (index>-1) {
      chatRooms[chatroom_name]["nicknames"].splice(index,1);
      //********
      //delete privateMessages[chatroom_name][nickname];
		//******************delete from private message ***********************
	  return true;
   }
	else{
		console.log(nickname +" is not found in "+chatroom_name);
		return false;
	}
}

function chatRoomExists(chatRoom){
	console.log("in chatRoomExists looking for: " + chatRoom);
	if(chatRoom in chatRooms){
		return true;
	}
	return false;
}

// Require the packages we will use:
var http = require("http"),
	socketio = require("socket.io"),
	fs = require("fs");
 
// Listen for HTTP connections.  This is essentially a miniature static file server that only serves our one file, client.html:
var app = http.createServer(function(req, resp){
	// This callback runs when a new connection is made to our HTTP server.
 
	fs.readFile("client.html", function(err, data){
		// This callback runs when the client.html file has been read from the filesystem.
 
		if(err) return resp.writeHead(500);
		resp.writeHead(200);
		resp.end(data);
	});
});
app.listen(3456);
 
// Do the Socket.IO magic:
var io = socketio.listen(app);
io.sockets.on("connection", function(socket){
	// This callback runs when a new Socket.IO connection is established.
	
	socket.on('message_to_server', function(data) {
	
		console.log("reading in the following message from the client");
		console.log(data);
		
		var nickname = data["nickname"];
		var message = data["message"];
		var current_room = data["currentRoom"];
		
		var message_str = nickname + ": " + message;
		
		console.log("current room to add: " + current_room);
		console.log("message str to add: " + message_str);
		
		console.log("current_room before");
      	console.log(chatRooms[current_room]);
	
		// This callback runs when the server receives a new message from the client.
      chatRooms[current_room]["messages"].push(message_str);
      
      console.log("current_room after");
      console.log(chatRooms[current_room]);
      
      	var object = new Object();
      	object["chatroom_data"] = chatRooms;
      	object["chatroom_name"] = current_room;
      
		io.sockets.emit("message_to_client", object); // broadcast the message to other users
	});
	
	socket.on("login", function(data){
		// assuming this is passed only to the client who logged on
		var response ={};
		response["nickname"] = data["nickname"];
		response["currentUsers"]= currentOnlineUsers;
		if (currentOnlineUsers.indexOf(data["nickname"]) < 0 ) {
         currentOnlineUsers.push(data["nickname"]);
			response["success"] = true;
			response["chatrooms_info"]= chatRooms;
			
			io.sockets.emit("login_response", response);
      }
		else{
			response["success"] = false;
			socket.emit("login_response",response);
		}
		
		
		
	});
	
	socket.on("logout", function(data){
		// assuming this is passed only to the client who logged on
		var response ={};
		var index = currentOnlineUsers.indexOf(data["nickname"]);
		if (index < 0 ) {
			response["success"] = false;
      }
		else{
			currentOnlineUsers.splice(index,1);
			response["success"] = true;
		}
		response["nickname"] = data["nickname"];
		response["chatrooms_info"]= chatRooms;
		response["currentUsers"] = currentOnlineUsers;
		io.sockets.emit("logout_response", response);
	});
	
	
	socket.on("add_chatroom", function(data){
		console.log("READ IN ADD CHATROOM FROM SOCKET");
	
		var new_chatroom_name = data["new_chatroom"];
		var chatroom_creator = data["creator_nickname"];
		
		console.log("new chatroom name: " + new_chatroom_name);
		console.log("chatroom creator: " + chatroom_creator);
		
		var roomPassword = null;
		if ("password" in data) { //if a password was sent in the request, then the room is private
         roomPassword = data["password"];
         console.log("haspassword");
        }
		
		var response = {};
		
		if(addChatRoom(new_chatroom_name, chatroom_creator, roomPassword)){
			console.log("chatroom was added successfully");
		
			response["success"] = true;
			response["chatroom_info"] = chatRooms;
			response["creator"] = chatroom_creator;
			console.log("response back to client is the following");
			console.log(response);
			response["currentUsers"] = currentOnlineUsers;
			io.sockets.emit("add_chatroom_response", response);
		}
		else{
			response["success"] = false;
			console.log("chatroom was not added");
			console.log(response);
			socket.emit("add_chatroom_response", response)
		}
	});
	
	socket.on("send_private_message", function(data){
		console.log("server received pm");
		
		var private_message = data["message"];
		var sender = data["sender"];
		var receiver = data["receiver"];
		var currentRoom = data["currentRoom"];
		
		// editing private_message
		private_message = "From " + sender + ": " + private_message;
		
		if(currentRoom in privateMessages){
			// this if statement is not working correctly
			if(receiver in privateMessages[currentRoom]){			
				privateMessages[currentRoom][receiver].push(private_message);
			}
			else{
				console.log("2: RECEIVER WAS NOT IN PRIVATE MESSAGES");
				
				console.log("receiver was: " + receiver);
				
				var pm_arr = new Array();
				pm_arr.push(private_message);
				
				privateMessages[currentRoom][receiver] = pm_arr;
			}
			
		}
		else{
			// privateMessages --> hash of chatroom names -- > hash of receivers -- > array of messages 
			receivers = {};
			//var pm_arr = [private_message];
			var pm_arr = new Array();
			pm_arr.push(private_message);
			
			receivers[receiver] = pm_arr;
			privateMessages[currentRoom] = receivers;
		}
		
		io.sockets.emit("private_message_response", privateMessages);	
	});
	
	socket.on('add_user', function(data) {
		var nickname = data["nickname"];
		var chatRoom = data["chatRoom"];
		
		addUserToChatRoom(data["nickname"], data["chatRoom"]);
		
		var response = {};
		response["updated_chatroom_name"] = chatRoom;
		response["chatroom_data"] = chatRooms;
		response["nickname_added"] = nickname;
		response["currentUsers"] = currentOnlineUsers; 
		console.log("add_user_response below: ");
		console.log(response);
		
		io.sockets.emit("add_user_response",response);
	});
	
	socket.on("kick_out_user", function(data){
		var user_to_kick_out = data["user_to_kick_out"];
		var chatroom_name = data["chatroom"];
		
		var index = chatRooms[chatroom_name]["nicknames"].indexOf(user_to_kick_out);
		
		chatRooms[chatroom_name]["nicknames"].splice(index, 1);
		
		var response = new Object();
		response["chatroom_data"] = chatRooms;
		response["user_kicked_out"] = user_to_kick_out;
		response["name_of_updated_chatroom"] = chatroom_name;
		response["user_kicking_out"] = data["user_kicking_out"];
		response["currentUsers"] = currentOnlineUsers;
		io.sockets.emit("kick_out_response", response);
	});
	
	socket.on("leave_chatroom", function(data){
		var nickname = data["nickname"];
		var chatroom_name = data["chatroom_name"];
		var response ={};
		if (leaveChatroom(nickname,chatroom_name)) {
         console.log("deleted "+nickname+" from "+chatroom_name+" nickname list");
			response["success"] = true;
			response["chatroom_info"] = chatRooms;
			response["nickname_leaving"] = nickname;
			response["chatroom_left"] = chatroom_name;
      }
		else{
			console.log("FAILED TO delete "+nickname+" from "+chatroom_name+" nickname list");
			response["success"] = false;
		}
		console.log("remaining users: "+chatRooms[chatroom_name]["nicknames"]);
		response["currentUsers"] = currentOnlineUsers;
		
		io.sockets.emit("leave_chatroom_response", response);
	});
	
	socket.on("ban_user", function(data){
		var user_to_ban = data["user_to_ban"];
		var chatroom_name = data["chatroom"];
		if(chatroom_name in bannedLists){
			bannedLists[chatroom_name].push(user_to_ban);
		}
		else{
			// if banned list for chatroom doesn't already exist
			var banned_users = new Array();
			banned_users.push(user_to_ban);
			bannedLists[chatroom_name] = banned_users;
		}
		console.log("added user to bannedlists");
		//console.log("tryna ban user "+user_to_ban);
		//console.log(bannedLists[chatroom_name]);
		//var output = new Object();
		//output["bannedLists"] = bannedLists;
		//output["currentUsers"] = currentOnlineUsers;
		io.sockets.emit("banned_response", bannedLists);
	});
});